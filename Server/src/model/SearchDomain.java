package model;

import java.util.HashMap;
/**
 * an interface for all of the games
 * @author itay&chen
 *
 */
public interface SearchDomain 
{
	State getStartState();//returns start state
	State getGoalState();//returns goal state
	HashMap<Action,State> getAllPossibleMoves(State current);//returns hash map of all the possible moves for a current state
	String getProblemDescription();//returns problem's description
}
