package model;

import java.util.ArrayList;
/**
 * This class is for the BreadthFirstSearch searching algorithm
 * @author itay&chen
 *
 */
public class BreadthFirstSearch extends AbstractSearcher
{
	/**
	 * this method is basically the brain of the algorithm, allows as to create a path between the start and the goal
	 */
	@Override
	public ArrayList<Action> search(SearchDomain domain)//returns the actions required to solve the given problem
	{
		super.setDomain(domain);
		State v=domain.getStartState();
		super.closedList.add(v);
		super.openList.add(v);
		while(!super.openList.isEmpty())
		{
			State t=super.openList.poll();
			if(t.equals(domain.getGoalState()))
				return super.backTrace(t);
			for(Action a:domain.getAllPossibleMoves(t).keySet())
			{
				State u=domain.getAllPossibleMoves(t).get(a);
				if(!super.closedList.contains(u))
				{
					u.setCameFrom(t);
					u.setCameWithAction(a);
					super.closedList.add(u);
					super.openList.add(u);
				}
			}
		}
		return null;
	}
	
}
