package model;

import java.util.ArrayList;
/**
 * This is the class for the BFS Searching algorithm
 * @author itay&chen
 *
 */
public class BFS extends AbstractSearcher
{
	/**
	 * this method is basically the brain of the algorithm, allows as to create a path between the start and the goal
	 */
	@Override
	public ArrayList<Action> search(SearchDomain domain) //returns the actions required to solve the given problem
	{
		super.setDomain(domain);
		super.openList.add(domain.getStartState());

		while(!super.openList.isEmpty())
		{
			State n=super.openList.poll();
			super.closedList.add(n);
			if(n.equals(domain.getGoalState()))
					return backTrace(n);
			for(Action a:domain.getAllPossibleMoves(n).keySet())
			{
				State s=domain.getAllPossibleMoves(n).get(a);
				if(!super.openList.contains(s)&&!super.closedList.contains(s))
				{
					s.setCameFrom(n);
					s.setCameWithAction(a);
					super.openList.add(s);
					s.setCost(n.getCost()+a.getPrice());
				}
				else
				{
					if(n.getCost()+a.getPrice()<s.getCost())
					{
						s.setCameFrom(n);
						s.setCameWithAction(a);
						if(!super.openList.contains(s))
							super.openList.add(s);
						else
						{
							State ts=s;
							super.openList.remove(s);
							super.openList.add(ts);
						}
					}
				}
			}
		}
		return null;
	}
}
