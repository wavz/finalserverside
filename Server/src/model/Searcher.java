package model;

import java.util.ArrayList;

/**
 * an interface for all the searching algorithms
 * @author itay&chen
 *
 */
public interface Searcher
{
	public ArrayList<Action> search(SearchDomain domain);//returns the actions required to solve the given problem
}
