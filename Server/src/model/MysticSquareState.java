package model;
/**
 * a class for each state in the MysticSquare game
 * @author wavz
 *
 */
public class MysticSquareState extends State
{

	private int rows, columns;//rows and columns of state's puzzle
	private int[][] puzzle;//state's puzzle
	/**
	 * creating a new state by cost and state(string)
	 * @param state
	 * @param cost
	 */
	public MysticSquareState(String state, double cost) 
	{
		super(state, cost);
		generatePuzzle(state);
	}
	/**
	 * copy constructor
	 * @param other
	 */
	public MysticSquareState(MysticSquareState other)
	{
		super(other.getState(),other.getCost());
		generatePuzzle(other.getState());
	}
	/**
	 * creating a new state by an array and a cost
	 * @param state
	 * @param cost
	 */
	public MysticSquareState(int[][] state,double cost)
	{
		super("",cost);
		super.setState(this.puzzleToState(state, state.length, state[0].length));
		this.rows=state.length;
		this.columns=state[0].length;
		this.puzzle=new int[rows][columns];
		for (int i = 0; i < puzzle.length; i++) 
		{
			for (int j = 0; j < state[0].length; j++) 
			{
				puzzle[i][j]=state[i][j];
			}
		}
	}
	/**
	 * a method that "creates" the moves for each state 
	 * @param a
	 * @return
	 */
	public boolean move(Action a)//get an action, if it fits to the possible actions, changes the puzzle by the move, and returns true, else, returns false
	{
		switch(a.getAction())
		{
			case "up":return moveUp();
			case "right":return moveRight();
			case "down":return moveDown();
			case "left":return moveLeft();
			default:return false;
		}
	}
	/**
	 * creating a new state after the user made a move
	 * @param a
	 * @return
	 */
	public MysticSquareState getANewStateByMove(Action a)//get an action and returns a stated created by launching the action on the current state
	{
		MysticSquareState s=new MysticSquareState(this);
		if(!s.move(a))
			return null;
		return s;
	}
	/**
	 * move up method
	 * @return true when we can move up
	 */
	private boolean moveUp()//launches the action 'up' on the state
	{	
		if(zeroRow()==rows-1)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR+1][zeroC];
		puzzle[zeroR+1][zeroC]=0;
		updateState();
		return true;
	}
	/**
	 * move right method
	 * @return true when we can move right
	 */
	private boolean moveRight()//launches the action 'right' on the state
	{
		if(zeroColumn()==0)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR][zeroC-1];
		puzzle[zeroR][zeroC-1]=0;
		updateState();
		return true;
	}
	/**
	 * move down method
	 * @return true when we can move down
	 */
	private boolean moveDown()//launches the action 'down' on the state
	{
		if(zeroRow()==0)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR-1][zeroC];
		puzzle[zeroR-1][zeroC]=0;
		updateState();
		return true;
	}
	/**
	 * move left method
	 * @return true when we can move left
	 */
	private boolean moveLeft()//launches the action 'left' on the state
	{
		if(zeroColumn()==columns-1)
			return false;
		int zeroR=zeroRow();
		int zeroC=zeroColumn();
		puzzle[zeroR][zeroC]=puzzle[zeroR][zeroC+1];
		puzzle[zeroR][zeroC+1]=0;
		updateState();
		return true;
	}
	/**
	 * 
	 * @return zero's x location
	 */
	private int zeroRow()//returns zero's row number in the array
	{
		for(int i=0;i<rows;i++)
		{
			for(int j=0;j<columns;j++)
			{
				if(puzzle[i][j]==0)
					return i;
			}
		}
		return -1;
	}
	/**
	 * 
	 * @return zero's y location
	 */
	private int zeroColumn()//returns zero's column number in the array
	{
		for(int i=0;i<rows;i++)
		{
			for(int j=0;j<columns;j++)
			{
				if(puzzle[i][j]==0)
					return j;
			}
		}
		return -1;
	}
	/**
	 * this method updates the state
	 */
	private void updateState()//updates the state
	{
		this.setState(puzzleToState(puzzle, rows, columns));
	}
	/**
	 * creting a string description of the state by a puzzle
	 * @param puzzle
	 * @param rows
	 * @param columns
	 * @return
	 */
	private String puzzleToState(int[][] puzzle,int rows,int columns)//returns a string description by puzzle
	{
		String state=rows+","+columns;
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				state+=","+puzzle[i][j];
			}
		}
		return state;
	}
	/**
	 * generates a puzzle by arguments
	 * @param args
	 */
	public void generatePuzzle(String args) //generates the puzzle by arguments
	{
		String[] splited = args.split(",");
		this.rows = Integer.parseInt(splited[0]);
		this.columns = Integer.parseInt(splited[1]);
		this.puzzle = new int[rows][columns];
		int k = 2;
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
			{
				puzzle[i][j] = Integer.parseInt(splited[k]);
				k++;
			}
	}
	public int getRows() //gets the rows
	{
		return rows;
	}
	public void setRows(int rows)//sets the rows
	{
		this.rows = rows;
	}
	public int getColumns() //returns the columns
	{
		return columns;
	}
	public void setColumns(int columns) //sets the columns
	{
		this.columns = columns;
	}
	public int[][] getPuzzle() //returns the puzzle
	{
		return puzzle;
	}
	@Override
	public String toString()//toString method
	{
		String s="\n";
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				s+="["+puzzle[i][j]+"]";
			}
			s+="\n";
		}
		return s;
	}
}
