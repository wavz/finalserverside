package model;
/**
 * The class that describes the problem
 */
import java.io.Serializable;

public class Problem implements Serializable
{
	private String domainArgs;
	private String algorithmName;
	/**
	 *
	 * @return the domain's arguments
	 */
	public String getDomainArgs() 
	{
		return domainArgs;
	}
	/**
	 * setting the domain arguments 
	 * @param domainArgs
	 */
	public void setDomainArgs(String domainArgs) 
	{
		this.domainArgs = domainArgs;
	}
	/**
	 * 
	 * @return algorithm's name
	 */
	public String getAlgorithmName() 
	{
		return algorithmName;
	}
	/**
	 * setting algorithm name
	 * @param algorithmName
	 */
	public void setAlgorithmName(String algorithmName) 
	{
		this.algorithmName = algorithmName;
	}
}
