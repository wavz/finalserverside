package model;

import java.util.HashMap;
/**
 * in this class we will create our domain like in a factory
 * @author itay&chen
 *
 */
public class DomainFactory
{
	
	private HashMap<String, DomainCreator> domains;//holds a transformation from description of a domain to a domain creator
	/**
	 * constructor that sets the possibilities of the domain in the hashMap 
	 */
	public DomainFactory()//constructor
	{
		domains=new HashMap<String, DomainCreator>();
		domains.put("Maze", new MazeCreator());
		domains.put("MysticSquare",new MysticSquareCreator());
	}
	/**
	 * private interface that declares the method "create"
	 * @author itay&chen
	 *
	 */
	private interface DomainCreator//an interface of a domain creator
	{
		SearchDomain create(String agrs);
	}
	/**
	 * a class that creats a new maze
	 * @author itay&chen
	 *
	 */
	private class MazeCreator implements DomainCreator//a domain creator that creates a maze
	{
		@Override
		public SearchDomain create(String args) 
		{
			return new MazeDomain(args);
		}		
	}
	/**
	 * a class that creats a new MysticSquare game
	 * @author wavz
	 *
	 */
	private class MysticSquareCreator implements DomainCreator//a domain creator that creates a puzzle
	{
		@Override
		public SearchDomain create(String args) 
		{
			return new MysticSquareDomain(args);
		}		
	}
	/**
	 * a method that creates the domain by the name and the arguments
	 * @param domainName
	 * @param args
	 * @return a new domain
	 */
	public SearchDomain createDomain(String domainName,String args)//the function creates a search domain by getting the name of the domain and a description of the arguments required
	{
		DomainCreator creator = domains.get(domainName);
		SearchDomain domain = null;
		if (creator != null)  
		{
			domain = creator.create(args);	
		}
		return domain;
	}
}



