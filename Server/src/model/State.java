package model;
/**
 * The class that describes a basic state
 * @author wavz
 *
 */
public abstract class State implements Comparable<State>
{
	private String state;//state's description
    private double cost;//state's cost  
    private State cameFrom;//the state that led to the current state
    private Action cameWithAction;//the action that led to the current state
    /**
     * creating a new state with a name and a cost
     * @param state
     * @param cost
     */
    public State(String state,double cost)//constructor
    {        
        this.state = state;
        this.cost = cost;
        this.cameFrom = null;
        this.cameWithAction=null;
    }
    /**
     * overriding the equals method
     */
    @Override
    public boolean equals(Object obj)//equals method
    {
        return state.equals(((State)obj).state);
    }
    /**
     * getting a state
     * @return
     */
	public String getState()//returns state's description
	{
		return state;
	}
	/**
	 * setting a state
	 * @param state
	 */
	public void setState(String state)//sets state's description
	{
		this.state = state;
	}
	/**
	 * getting the cost
	 * @return
	 */
	public double getCost()//returns state's cost
	{
		return cost;
	}
	/**
	 * seting the cost
	 * @param cost
	 */
	public void setCost(double cost)//sets state's cost
	{
		this.cost = cost;
	}
	/**
	 * getting the adrress of the previous state
	 * @return
	 */
	public State getCameFrom()//returns the state that led to the current state
	{
		return cameFrom;
	}
	/**
	 * setting the adrress of the previous state
	 * @param cameFrom
	 */
	public void setCameFrom(State cameFrom)//sets the state that led to the current state
	{
		this.cameFrom = cameFrom;
	}
	/**
	 * 
	 * @return the action that led to the current state
	 */
	public Action getCameWithAction()//returns the action that led to the current state
	{
		return cameWithAction;
	}
	/**
	 * sets the action that led to the current state
	 * @param cameWithAction
	 */
	public void setCameWithAction(Action cameWithAction)//sets the action that led to the current state
	{
		this.cameWithAction = cameWithAction;
	} 
	/**
	 * compareTo method
	 */
	@Override
	public int compareTo(State other)//compareTo method
	{
		if(this.equals(other))
			return 0;
		if(this.cost>other.cost)
			return 1;
		return -1;
	}
}
