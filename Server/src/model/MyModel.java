package model;

import java.util.ArrayList;
import java.util.Observable;
/**
 * this is the class that basically handles the problem and the solution and creating all the things we need to play   
 * @author wavz
 *
 */
public class MyModel extends Observable implements Model
{
	private SearchDomain domain;//the domain of the model
	private Searcher algorithm;//the specific algorithm used to solve domain's problem
	private SearchAlgorithmsFactory algorithmsFactory;//creates algorithms by choice
	private Solution solution;//the solution of the domain's problem
	private DomainFactory domainFactory;//creates domains by choice
	private Problem problem;
	/**
	 * constructor
	 */
	public MyModel()//constructor
	{
		algorithmsFactory = new SearchAlgorithmsFactory();
		domainFactory=new DomainFactory();
	}
	/**
	 * setting the problem
	 */
    public void setProblem(Problem problem)
    {
    	this.problem=problem;
    	String domainName=getDomainNameByArgs(problem.getDomainArgs());
    	selectAlgorithm(problem.getAlgorithmName(),domainName);
    	selectDomain(problem.getDomainArgs());
    }
    
    /**
     * getting the domain name by arguments
     * @param domainArgs
     * @return the domain name
     */
    private String getDomainNameByArgs(String domainArgs)
    {
    	String[] splited=domainArgs.split(":");
    	String domainName=splited[0];
    	return domainName;
    }
    /**
     * selcting the domain by arguments
     */
	@Override
	public void selectDomain(String args) //selects a domain and creates it by getting domain's arguments
	{
		String[] arr = args.split(":");
		System.out.println("SelectDomain-1:");
		String domainName = arr[0];
		String domainArgs = arr[1];
		System.out.println("SelectDomain-2:");
		System.out.println("SelectDomain-domain Name:"+domainName+"\n\n");
		System.out.println("SelectDomain-domain Args:"+domainArgs+"\n\n");
		domain = domainFactory.createDomain(domainName,domainArgs);
		System.out.println("SelectDomain-3:");
	}
	@Override
	
	/**
	 * creating the searching algorithm by the domain's name and the algorithm's name 
	 */
	public void selectAlgorithm(String algorithmName,String nameDomain) //selects an algorithm and creates it by getting algorithm's name and domain's name
	{
		algorithm = algorithmsFactory.createAlgorithm(algorithmName,nameDomain);
	}
	/**
	 * the method that solving our game/problem/domain
	 */
	@Override
	public void solveDomain() //solves and prints the domain
	{		
		String problemDescription = domain.getProblemDescription();
		//this.solution = SolutionManager.getInstance().getSolution(problemDescription);
		this.solution=null;
		//System.out.println(domain);
		if(solution==null)
		{
			long startTime = System.nanoTime();
			ArrayList<Action> actions = algorithm.search(domain);
			if(actions!=null)
				for(Action a:actions)
					System.out.println(a.getAction());
			
			long stopTime = System.nanoTime();
			solution = new Solution(actions);
			//SolutionManager.getInstance().addSolution(solution);
			System.out.println("total time:"+(stopTime - startTime));
		}
		this.setChanged();
		this.notifyObservers();
	}
	/**
	 * getting the solution
	 */
	@Override
	public Solution getSolution() //gets the solution of the domain, if there is one
	{
		return solution;
	}
	@Override
	
	/**
	 * selecting the algorithm by arguments
	 */
	public void selectAlgorithm(String args) 
	{
		if(args.indexOf(';')==-1)
			selectAlgorithm(args,"");
		else
		{
			String[] split=args.split(";");
			String algorithmName=split[0];
			String domainName=split[1];
			selectAlgorithm(algorithmName,domainName);
		}
	}
	/**
	 * solving the problem in threads
	 */
	@Override
	public void doTask() 

	{	
		solveDomain();
	}
	/**
	 * 
	 * @return the domain
	 */
	public SearchDomain getDomain()
	{
		return domain;
	}
}
