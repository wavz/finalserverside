package model;
/**
 * a class for the heuristic function of the maze
 * @author itay&chen
 *
 */
public class MazeHeuristic implements Heuristic
{
	/**
	 * the heristic method of the maze
	 */
	@Override
	public double heuristic(State current, State goal)//returns the heuristic value for current and goal state
	{
		int totalX=((MazeState)goal).getRow()-((MazeState)current).getRow();
		int totalY=((MazeState)goal).getColumn()-((MazeState)current).getColumn();
		return Math.sqrt(Math.pow(totalX, 2)+Math.pow(totalY, 2));
	}

}
