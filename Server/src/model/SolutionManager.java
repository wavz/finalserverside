package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

/**
 * the solution manager
 * @author itay&chen
 *
 */
public class SolutionManager
{
	private HashMap<String, Solution> solutionsMap;//holds a transformation from problem's description to a domain's solution
	private static SolutionManager instance = null;//the instance of the solution manager
	private static final String FILE_NAME = "resources/solutions.dat";//save file's name
	/**
	 * default constructor
	 */
	private SolutionManager() //default constructor
	{	
		solutionsMap = new HashMap<String, Solution>();
		//loadSolutionFromFile();
	}
	/**
	 * 
	 * @return the object itself( becuase this is a singelton pattern)
	 */
	public static SolutionManager getInstance() //returns the singleton instance of solution manager
	{
		if (instance == null) 
		{
			instance = new SolutionManager();			
		}
		return instance;
	}
	/**
	 * adding a solution to the hashMap
	 * @param solution
	 */
	public void addSolution(Solution solution)//adds a solution to the solutions' hash map
	{
		solutionsMap.put(solution.getProblemDescription(), solution);
	}
	/**
	 * getting a solution
	 * @param problemDescription
	 * @return
	 */
	public Solution getSolution(String problemDescription) //gets a solution from the hash map by problem's description
	{
		return solutionsMap.get(problemDescription);
	}
	/**
	 * saving the solution into a file
	 */
	public void saveSolutionsInFile()//saves the solutions in a file
	{
		FileOutputStream out = null;
		ObjectOutputStream oos = null;
		try
		{
			out = new FileOutputStream(FILE_NAME);
			oos = new ObjectOutputStream(out);
			oos.writeObject(solutionsMap);
			
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (out != null)
			{
				try 
				{
					out.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * loading solution from a file
	 */
	public void loadSolutionFromFile()//loads past solutions from a file
	{
		File f=new File(FILE_NAME);
		if(f.exists())
		{
			try(BufferedReader bf=new BufferedReader(new FileReader(FILE_NAME)))
			{
				ObjectInputStream in=new ObjectInputStream(new FileInputStream(FILE_NAME));
				solutionsMap=(HashMap<String, Solution>)in.readObject();
				in.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}catch(ClassNotFoundException e)
			{
				e.printStackTrace();
			}
		}
	}
}
