package model;
/**
 * interface for all the searching algorithms that are using heuristic functions
 * @author itay&chen
 *
 */
public interface Heuristic
{
	public double heuristic(State current, State goal);//returns the heuristic value for current and goal state
}
