package model;

import java.util.ArrayList;
import java.util.Stack;
/**
 * This class is for the DFS searching algorithm
 * @author itay&chen
 *
 */
public class DFS extends AbstractSearcher
{
	/**
	 * this method is basically the brain of the algorithm, allows as to create a path between the start and the goal
	 */
	@Override
	public ArrayList<Action> search(SearchDomain domain)//returns the actions required to solve the given problem
	{
		super.setDomain(domain);
		Stack<State> s=new Stack<State>();
		State v=domain.getStartState();
		s.push(v);
		super.closedList.add(v);
		while(!s.isEmpty())
		{
			v=s.pop();
			if(v.equals(domain.getGoalState()))
				super.backTrace(v);
			if(!super.closedList.contains(v))
			{
				super.closedList.add(v);
				for(Action a:domain.getAllPossibleMoves(v).keySet())
				{
					State w=domain.getAllPossibleMoves(v).get(a);
					w.setCameFrom(v);
					w.setCameWithAction(a);
					s.push(w);
				}
			}
		}
		if(v.equals(domain.getGoalState()))
			super.backTrace(v);
		return null;
	}

}
