package model;
/**
 * the class for the heuristic function of the MysticSquare game
 * @author itay&chen
 *
 */
public class MysticSquareHeuristic implements Heuristic
{
	/**
	 * the heuristic function of the MysticSquare game
	 */
	@Override
	public double heuristic(State current, State goal)//returns the heuristic value for current and goal state
	{
		int rows=((MysticSquareState)current).getRows();
		int columns=((MysticSquareState)current).getColumns();
		int h=0;
		for (int i = 0; i < rows; i++) 
		{
			for (int j = 0; j < columns; j++) 
			{
				int[][] currentArray=((MysticSquareState)current).getPuzzle();
				int[][] goalArray=((MysticSquareState)goal).getPuzzle();
				if(currentArray[i][j]!=goalArray[i][j])
					h++;
			}
		}
		return h;
	}
}