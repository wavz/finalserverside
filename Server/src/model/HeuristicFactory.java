package model;

import java.util.HashMap;
/**
 * this is a class that creates a new heuristic function for each domain like in a factory
 * @author itay&chen
 *
 */
public class HeuristicFactory 
{
	private HashMap<String, HeuristicCreator> heuristics;//holds a transformation from description of a domain to an heuristic creator
	/**
	 * constructor that creates a new hashMap and sets the names of the games(domains) 
	 */
	public HeuristicFactory()//constructor
	{
		heuristics=new HashMap<String, HeuristicCreator>();
		heuristics.put("Maze", new MazeHeuristicCreator());
		heuristics.put("MysticSquare", new MysticSquareHeuristicCreator());
	}
	/**
	 * an interface that declares the function create( a new heuristic function)
	 * @author itay&chen
	 *
	 */
	private interface HeuristicCreator//an interface of an heuristic creator
	{
		Heuristic create();
	}
	/**
	 * the class that creates a new heuristic function for the maze
	 * @author itay&chen
	 *
	 */
	private class MazeHeuristicCreator implements HeuristicCreator//an heuristic creator that creates a maze heuristic
	{
		@Override
		public Heuristic create() 
		{
			return new MazeHeuristic();
		}	
	}
	/**
	 * the class that creates a new heuristic function for the MysticSquare game
	 * @author itay&chen
	 *
	 */
	private class MysticSquareHeuristicCreator implements HeuristicCreator//an heuristic creator that creates a puzzle eight heuristic
	{
		@Override
		public Heuristic create() 
		{
			return new MysticSquareHeuristic();
		}
	}
	/**
	 * a method that creates a new heuristic function by the domain's name
	 * @param domainName
	 * @return
	 */
	public Heuristic createHeuristic(String domainName)//the function creates an heuristic by getting the domain's name
	{
		HeuristicCreator creator = heuristics.get(domainName);
		Heuristic heuristic = null;
		if (creator != null)  
		{
			heuristic = creator.create();			
		}
		return heuristic;
		
	}
}
