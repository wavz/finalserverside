package model;
/**
 * the class that manges the solution
 */
import java.io.Serializable;
import java.util.ArrayList;

public class Solution implements Serializable
{
	private ArrayList<Action> actions;//the actions required to solve the domain's problem
	private String problemDescription;//the problem's description
	/**
	 * constructor
	 * @param actions
	 */
	public Solution(ArrayList<Action> actions)//constructor
	{
		setActions(actions);
	}
	/**
	 * 
	 * @return a list of actions
	 */
	public ArrayList<Action> getActions()//returns the actions required to solve the domain's problem by an array list
	{
		return actions;
	}
	/**
	 * setting the list of actions
	 * @param actions
	 */
	public void setActions(ArrayList<Action> actions) //sets the actions required to solve the domain's problem
	{
		this.actions=actions;
	}
	/**
	 * 
	 * @return the problem description
	 */
	public String getProblemDescription()//gets problem's description
	{
		return problemDescription;
	}
	/**
	 * setting the problem description
	 * @param problemDescription
	 */
	public void setProblemDescription(String problemDescription)//sets problem's description
	{
		this.problemDescription = problemDescription;
	}
}
