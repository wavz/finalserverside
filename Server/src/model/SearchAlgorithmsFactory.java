package model;

import java.util.HashMap;
/**
 * the class that creates all of the algorithms
 * @author itay&chen
 *
 */
public class SearchAlgorithmsFactory
{
	private HashMap<String, AlgorithmCreator> algorithms;//holds a transformation from description of an algorithm to an algorithm creator
	/**
	 * constructor
	 */
	public SearchAlgorithmsFactory()//constructor
	{
		algorithms = new HashMap<String, AlgorithmCreator>();
		algorithms.put("BFS", new BFSCreator());
		algorithms.put("Astar", new AstarCreator());
		algorithms.put("BreadthFirstSearch", new BreadthFirstSearchCreator());
		algorithms.put("DFS", new DFSCreator());
		//algorithms.put("Dijkstra", new DijkstraCreator());
	}
	/**
	 * creating the algorithm by algorithm's name and domain's name
	 * @param algorithmName
	 * @param nameDomain
	 * @return
	 */
	public Searcher createAlgorithm(String algorithmName,String nameDomain)//creates an algorithm by getting its name and domain's name it solves
	{
		AlgorithmCreator creator = algorithms.get(algorithmName);
		Searcher searcher = null;
		if (creator != null)  {
			searcher = creator.create(nameDomain);			
		}
		return searcher;
	}
	/**
	 * algorithm creator interface
	 * @author itay&chen
	 *
	 */
	private interface AlgorithmCreator//an interface of an algorithm creator
	{
		Searcher create(String nameDomain);
	}
	/**
	 * BFS creator
	 * @author itay&chen
	 *
	 */
	private class BFSCreator implements AlgorithmCreator//an algorithm creator that creates a BFS algorithm
	{
		@Override
		public Searcher create(String nameDomain)
		{
			return new BFS();
		}		
	}
	/**
	 * aStar creator
	 * @author itay&chen
	 *
	 */
	private class AstarCreator implements AlgorithmCreator//an algorithm creator that creates an A* algorithm by getting domain's name
	{
		@Override
		public Searcher create(String nameDomain)
		{
			return new Astar(nameDomain);
		}		
	}
	/**
	 * BreadthFirstSearchCreator creator
	 * @author itay&chen
	 *
	 */
	private class BreadthFirstSearchCreator implements AlgorithmCreator//an algorithm creator that creates a Breadth First Search algorithm
	{
		@Override
		public Searcher create(String nameDomain)
		{
			return new BreadthFirstSearch();
		}		
	}
	/**
	 * DFS creator
	 * @author itay&chen
	 *
	 */
	private class DFSCreator implements AlgorithmCreator//an algorithm creator that creates a DFS algorithm
	{
		@Override
		public Searcher create(String nameDomain)
		{
			return new DFS();
		}		
	}
	
}
