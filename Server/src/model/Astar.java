package model;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * This is the class for the aStar searching algorithms
 * @author itay&chen
 *
 */
public class Astar extends AbstractSearcher
{
	Heuristic h;//holds the heuristic function, that we got by object injection
	HashMap<State,Double> gScore;//holds the g score of all the nodes
	/**
	 * constructor that sets the right heuristic function and creating a new hashMap
	 * @param h
	 */
	public Astar(Heuristic h) //constructor
	{
		this.h=h;
		gScore=new HashMap<State,Double>();
	}
	/**
	 * constructor by arguments
	 * @param args
	 */
	public Astar(String args)//constructor
	{
		this.h=(new HeuristicFactory()).createHeuristic(args);
		gScore=new HashMap<State,Double>();
	}
	/**
	 * this method is basically the brain of the algorithm, allows as to create a path between the start and the goal
	 */
	@Override
	public ArrayList<Action> search(SearchDomain domain)//returns the actions required to solve the given problem
	{
		super.setDomain(domain);
		super.openList.add(domain.getStartState());
		gScore.put(domain.getStartState(), (double) 0);
		double hFromStartToGoal=h.heuristic(domain.getStartState(), domain.getGoalState());
		domain.getStartState().setCost(gScore.get(domain.getStartState())+hFromStartToGoal);
		while(!super.openList.isEmpty())
		{
			State current=super.openList.peek();
			if(current.equals(domain.getGoalState()))
				return backTrace(current);
			if(!gScore.containsKey(current))
				gScore.put(current, (double) 0);
			super.closedList.add(current);
			super.openList.remove(current);
			for(Action a: domain.getAllPossibleMoves(current).keySet())
			{
				State neighbor=domain.getAllPossibleMoves(current).get(a);
				if(!gScore.containsKey(neighbor))
					gScore.put(neighbor, (double) 0);
				if(super.closedList.contains(neighbor))
					continue;
				double tentativeGScore=gScore.get(current)+a.getPrice();
				if(!super.openList.contains(neighbor)||tentativeGScore<gScore.get(neighbor))
				{
					neighbor.setCameFrom(current);
					neighbor.setCameWithAction(a);
					gScore.put(neighbor, tentativeGScore);
					neighbor.setCost(gScore.get(neighbor)+h.heuristic(neighbor, domain.getGoalState()));
					if(!super.openList.contains(neighbor))
						super.openList.add(neighbor);
				}
			}
		}
		return null;
	}
	/**
	 * setting the heuristic function
	 * @param h
	 */
	public void setHeuristic(Heuristic h)//sets the heuristic value
	{
		this.h=h;
	}

}
