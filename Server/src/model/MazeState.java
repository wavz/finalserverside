package model;
/**
 * A state of every square in the maze
 * @author wavz
 *
 */
public class MazeState extends State
{
	int row,column;//amount of rows and columns
	boolean blocked;//if the state is blocked, this data member will be true, or else, false
	/**
	 * creating a new maze state by position(x,y)
	 * @param state
	 * @param row
	 * @param column
	 */
	public MazeState(String state,int row,int column)//constructor
	{
		super(state, 0);
		this.row=row;
		this.column=column;
		this.blocked=false;
	}
	/**
	 * copy constructor
	 * @param other
	 */
	public MazeState(MazeState other)//constructor
	{
		super(other.getState(),other.getCost());
		this.row=other.row;
		this.column=other.column;
		this.blocked=other.blocked;
	}
	public int getRow()//returns the row
	{
		return row;
	}
	public void setRow(int row)//sets the row
	{
		this.row = row;
	}
	public int getColumn()//returns the column
	{
		return column;
	}
	public void setColumn(int column)//sets the column
	{
		this.column = column;
	}
	/**
	 * boolean method that checks if the state is a wall or not
	 * @return true when the state is a wall
	 */
	public boolean isBlocked()//returns true for blocked state, or else, returns false
	{
		return this.blocked;
	}
	/**
	 * a simple method that makes a state to a wall
	 */
	public void block()//blocks the state
	{
		this.blocked=true;
	}
	/**
	 * unblocking the state
	 */
	public void unBlock()//unblocks the state
	{
		this.blocked=false;
	}
	/**
	 * overriding the toString method
	 */
	@Override
	public String toString()//toString method
	{
		int block=0;
		if(blocked)
			block=1;
		return "["+block+"]";
	}
}
