package model;

import java.util.ArrayList;

import java.util.PriorityQueue;
/**
 * This is the class with the common methods for all the Searching algorithms
 * @author itay&chen
 *
 */
public abstract class AbstractSearcher implements Searcher
{
	PriorityQueue<State> openList;//list of unchecked nodes 
	PriorityQueue<State> closedList;//list of checked nodes
	SearchDomain domain;//external domain
	/**
	 * constructor - creating a new priority queues ( closed list and open list )
	 */
	public AbstractSearcher()//constructor
	{
		this.openList=new PriorityQueue<State>();
		this.closedList=new PriorityQueue<State>();
	}
	/**
	 * setting the domain for the searching algorithms
	 * @param domain
	 */
	public void setDomain(SearchDomain domain)
	{
		this.domain=domain;
	}
	/**
	 * The most important method in the project
	 * @param current
	 * @return a list of actions that the start state need to take in order to reach to the target
	 */
	public ArrayList<Action> backTrace(State current)//returns list of the actions required to get the current state
	{
		State temp=current;
		ArrayList<Action> actions=new ArrayList<Action>();
		while(temp.getCameFrom()!=null)
		{
			actions.add(0,temp.getCameWithAction());
			temp=temp.getCameFrom();
		}
		return actions;
	}
}
