package model;

import java.util.Observer;

import tasks.Task;
/**
 * the interface of our model
 * @author itay&chen
 *
 */
public interface Model extends Task
{
	// we don't think we need a java doc for every method in the interface
	void selectDomain(String domainName);//selects a domain by getting domain's name
	void selectAlgorithm(String algorithmName,String domainName);//selects an algorithm by getting alrorithm's name and domain's name
	void selectAlgorithm(String args);//selects algorithm by arguments
	void solveDomain();//solves the domain, and prints it 
	Solution getSolution();//achieves the solution of the domain, if there is one
	void addObserver(Observer o);//adds observer
	void setProblem(Problem problem);
}
