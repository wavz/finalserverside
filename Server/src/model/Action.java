package model;

import java.io.Serializable;
/**
 * This is a class for the actions(moves) that needs to be taken from the start
 * @author itay&chen
 *
 */
public class Action implements Serializable
{
	String action;//description of action
	double price;//price of action
		/**
		 * constructor that sets the description of the move( like up/down etc.)
		 * @param description
		 */
		public Action(String description) //constructor
		{
			this.action=description;
		}
		/**
		 * constructor that sets the description and the cost between every move
		 * @param description
		 * @param price
		 */
		public Action(String description,double price)//constructor
		{
			this.action=description;
			this.price=price;
		}
		/**
		 * overriding the equals method 
		 */
		@Override 
		public boolean equals(Object obj)//equals method
		{
			return action.equals(((Action)obj).action);
		}
		/**
		 * overriding the hashcode method
		 */
		@Override
		public int hashCode()//hash code function (uses the string class hash code function)
		{
			return action.hashCode();
		}	
		/**
		 * overriding the toString method
		 */
		@Override
		public String toString()//toString method
		{
			return action;
		}
		/**
		 * getters & setters
		 * @return the price of the move
		 */
		public double getPrice()//returns the price of the action
		{
			return price;
		}
		/**
		 * getters & setters
		 * @param price
		 */
		
		public void setPrice(double price)//sets the price of the action
		{
			this.price=price;
		}
		/**
		 * 
		 * @return the move ( as a string )
		 */
		public String getAction()//returns action's description
		{
			return action;
		}
		/**
		 * setting the action by need
		 * @param action
		 */
		public void setAction(String action)//sets action's description
		{
			this.action = action;
		}
}
