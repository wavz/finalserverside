package config;

import java.beans.XMLDecoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
/**
 * the class that handles the properties
 * @author itay&chen
 *
 */
public class HandleProperties
{
	private static final String FILE_NAME = "resources/Properties.xml";
	/**
	 * a method that allows us to read the properties file
	 * @return the server properties
	 */
	public static ServerProperties readProperties()
	{
		XMLDecoder decoder = null;
		try 
		{
			decoder = new XMLDecoder(new FileInputStream(FILE_NAME));
			ServerProperties properties = (ServerProperties)decoder.readObject();
			return properties;
		} 
		catch (FileNotFoundException e)
		{
			//e.printStackTrace();
			System.out.println("HandleProperties-24");
		} 
		finally
		{
			decoder.close();
		}
		return null;
	}
}
