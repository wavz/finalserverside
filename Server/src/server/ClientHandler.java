package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import model.Model;
import model.MyModel;
import model.MysticSquareDomain;
import model.Problem;
import model.Solution;
import tasks.Task;
/**
 * the class that handles the clients
 * @author itay&chen
 *
 */
public class ClientHandler implements Task
{
	private Socket socket;
	/**
	 * creating the server with a new socket
	 * @param socket
	 */
	public ClientHandler(Socket socket)
	{
		this.socket = socket;
	}
	/**
	 * the method that starts data exchanges with the clients
	 */
	@Override
	public void doTask()
	{
		ObjectInputStream in = null;
		ObjectOutputStream out = null;
		try 
		{	
			in = new ObjectInputStream(socket.getInputStream());
			Problem problem = (Problem)in.readObject();
			//System.out.println("Got new problem: " + problem.getDomainName());
			Model model = new MyModel();
			model.setProblem(problem);
			model.solveDomain();
			Solution solution = model.getSolution();
			out = new ObjectOutputStream(socket.getOutputStream());
			//System.out.println("Found solution: " + solution.getProblemDescription());
			out.writeObject(solution);
		} 
		catch (IOException e)
		{			
			//e.printStackTrace();
			System.out.println("ClientHandler-49");
		}
		catch (ClassNotFoundException e)
		{
			//e.printStackTrace();
			System.out.println("ClientHandler-54");
		} 
		finally 
		{
			try 
			{
				out.close();
				in.close();
				socket.close();
			}
			catch (IOException e) 
			{
				//e.printStackTrace();
				System.out.println("ClientHandler-67");
			}			
		}	
	}	
}
