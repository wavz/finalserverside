package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import config.HandleProperties;
import config.ServerProperties;
import tasks.TaskRunnable;
/**
 * the class that describes the server( with tcp protocol)
 * @author itay&chen
 *
 */
public class MyTCPIPServer 
{
	private ServerSocket server;
	private ExecutorService executor;
	private Thread thread;
	private int port;
	private int numOfClients;
	/**
	 * default constructor	
	 */
	public MyTCPIPServer() 
	{
		ServerProperties properties = HandleProperties.readProperties();
		this.port = properties.getPort();
		this.numOfClients = properties.getNumOfClients();
	}
	/**
	 * creating the server by port and number of clients
	 * @param port
	 * @param numOfClients
	 */
	public MyTCPIPServer(int port, int numOfClients)
	{
		this.port = port;
		this.numOfClients = numOfClients;
	}
	/**
	 * starting the server
	 */
	public void startServer() 
	{
		try 
		{
			server = new ServerSocket(port);			
			server.setSoTimeout(1000000);
			
			executor = Executors.newFixedThreadPool(numOfClients);
		
			thread = new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					while(!Thread.interrupted()) 
					{
						try
						{
							Socket socket = server.accept();
							//System.out.println("Got new connection");
							if (socket != null) 
							{
								ClientHandler handler = new ClientHandler(socket);
								executor.submit(new TaskRunnable(handler));
							}
						} 
						catch (IOException e) 
						{
							//e.printStackTrace();
							System.out.println("MyTCPIPServer-63");
						}	
						catch (Exception e) 
						{
							System.out.println("MyTCPIPServer-67");
						}
					}				
				}			
			});
		thread.start();
		} 
		catch (IOException e1) 
		{
			System.out.println("the connection is already in use");
		}		
	}
	/**
	 * stoping the server
	 */
	public void stopServer()
	{		
		try 
		{
			thread.interrupt();
			executor.shutdownNow();
			server.close();
		} 
		catch (IOException e)
		{
			//e.printStackTrace();
			System.out.println("MyTCPIPServer-90");
		}
	}
}
